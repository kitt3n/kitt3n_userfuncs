<?php
declare(strict_types = 1);

namespace KITT3N\Kitt3nUserfuncs\ExpressionLanguage\Functions;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;
use Symfony\Component\Filesystem\Filesystem;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class CustomTyposcriptConditionFunctionsProvider implements ExpressionFunctionProviderInterface
{

    /**
     * @return ExpressionFunction[] An array of Function instances
     */
    public function getFunctions()
    {
        return [
            $this->getFileExistence(),
            $this->getLoadedExtension(),
        ];
    }

    /**
     * Shortcut function to access field values
     *
     * @return \Symfony\Component\ExpressionLanguage\ExpressionFunction
     */
    public function getFileExistence(): ExpressionFunction
    {
        return new ExpressionFunction('fileExists', function () {
            // Not implemented, we only use the evaluator
        }, function ($arguments, $str) {

            $sDocumentTargetPath = '';
            $sServerDocumentRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/';
            $fs = new Filesystem();

            if(strpos($str, 'EXT:') !== false){
                $sFilePathFromDocRoot = 'typo3conf/ext/' . str_replace('EXT:','',$str);
            } else {
                $sFilePathFromDocRoot = ltrim($str, '/');
            }

            $sDocumentTargetPath .= $sServerDocumentRoot . $sFilePathFromDocRoot;

            if ($fs->exists($sDocumentTargetPath)) {
                return true;
            } else {
                $x = 0;
                return false;
            }

        });
    }


    /**
     * Shortcut function to access field values
     *
     * @return \Symfony\Component\ExpressionLanguage\ExpressionFunction
     */
    protected function getLoadedExtension(): ExpressionFunction
    {
        return new ExpressionFunction('isLoaded', function () {
            // Not implemented, we only use the evaluator
        }, function ($arguments, $key) {

            if(!isset($GLOBALS['TYPO3_LOADED_EXT'][$key])){
                return false;
            } else {
                return true;
            }

        });
    }
}
